Batch CSV Import

This project is intended to be a helper for developers that need to implement Batch API on a CSV file importer.

All you need to do is call to create a batch using the function batch_csv_import_batch($options), and then pass this batch to batch_set function.

Usage example with all available options:

$options = [
    'title' => t('Batch title'),
    'function' => 'process_function_name',
    'file_path' => 'csv_file_filepath',
    'function_path' => 'process_function_filepath',
    'step' => 50,
    'delimiter' => '|',
    'skip count' => 1,
    'preprocess callback' => 'preprocess_function_name',
    'preprocess arguments' => [],
    'messages' => [
      'success' => [
        'message' => " items imported.",
        'type' => 'status',
      ],
      'failed' => [
        'message' => " items failed.",
        'type' => 'error',
      ],
  ],
];
$batch = batch_csv_import_batch($options);
batch_set($batch);
